#ifndef JSONZRODLO_H
#define JSONZRODLO_H

#include <QObject>
#include <QJsonObject>

class JsonZrodlo : public QObject
{
    Q_OBJECT
public:
    explicit JsonZrodlo(QObject *parent = nullptr);

    QJsonObject &setJsonObject(QJsonObject &Obj);
    void setJsonNetwork(QString &Net);
    QString JsonNetwork();

signals:

private:
    QString JsonStr;
    QJsonObject JsonObj;
};

#endif // JSONZRODLO_H
