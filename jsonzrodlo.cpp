#include "jsonzrodlo.h"
#include <QDebug>

JsonZrodlo::JsonZrodlo(QObject *parent) : QObject(parent), JsonStr("Default String")
{
}

QJsonObject &JsonZrodlo::setJsonObject(QJsonObject &Obj)
{
    Obj["network"] = JsonStr;

    JsonObj = Obj;

    QJsonObject::iterator i;

    for(i = JsonObj.begin(); i != JsonObj.end(); ++i){
        qDebug()<< i.key() << i.value().toString();
    }

    qDebug()<< Qt::endl;

    return JsonObj;
}

void JsonZrodlo::setJsonNetwork(QString &Net)
{
    JsonStr = Net;

    QJsonObject::iterator i;

    for(i = JsonObj.begin(); i != JsonObj.end() ; ++i){
        if(i.key() == "network"){
            JsonObj.insert(i.key(), JsonStr);
        }
    }
}

QString JsonZrodlo::JsonNetwork()
{
    QString str;

    foreach(str, JsonObj.keys())
    {
        qDebug()<< str << JsonObj.value(str).toString();
    }

    return JsonStr;
}
