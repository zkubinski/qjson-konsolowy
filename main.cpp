#include <iostream>
#include <QJsonObject>
#include <QDebug>

#include "jsonzrodlo.h"
#include "jsoncel.h"

using namespace std;

QJsonObject &funkcja (QJsonObject &Obj)
{
    Obj.insert("klucz-1", "zmieniona wartosc");
    Obj.insert("nowy-klucz", "udana zmiana");

    return Obj;
}

int main()
{
    QString str;
    QJsonObject myObj;

    myObj.insert("klucz-1","wartosc");

    qDebug()<< "json przed zmiana";

    foreach(str, myObj.keys())
    {
        qDebug()<< str << myObj.value(str).toString();
    }

    qDebug()<< Qt::endl;

    qDebug()<< "json po zmianie";

    funkcja(myObj);

    foreach(str, myObj.keys())
    {
        qDebug()<< str << myObj.value(str).toString();
    }

    qDebug()<< Qt::endl;

    //----------------------

    qDebug()<< "gdzies dalej w kodzie...";

    QJsonObject myObj2;

    qDebug()<< Qt::endl;

    myObj2.insert("test", "wartosc");

    myObj = funkcja(myObj2);

    qDebug()<< "czy obiekt jest dostepny dla wszystkich ?" << Qt::endl;

    foreach(str, myObj.keys()){
        qDebug()<< str << myObj.value(str).toString();
    }

    return 0;
}
