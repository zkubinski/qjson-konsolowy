#ifndef JSONCEL_H
#define JSONCEL_H

#include <QObject>
#include <QJsonObject>

#include "jsonzrodlo.h"

class JsonCel : public QObject
{
    Q_OBJECT
public:
    explicit JsonCel(QObject *parent = nullptr);

    void setJsonSetting(QJsonObject &obj);
    void JsonSetting();

private:
    QJsonObject JsonObj;
    JsonZrodlo zrodlo;

signals:

};

#endif // JSONCEL_H
